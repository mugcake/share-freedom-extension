import { generateArray, gettingItems, saveItems } from './utils';
import * as instance from './instances';

const items = generateArray(instance);
const containerItems = document.getElementById('root');
containerItems.innerHTML = '';

items.forEach((value) => {
  const name = value.name;
  const html = `
  <div class="flex">
    <span class="span-custom">${name}:</span>
    <input id="${name.toLowerCase()}-host" class="text-custom" type="text" >
    <div class="bg-gray-500" >
      <input class="checkbox-custom" type="checkbox" id="${name.toLowerCase()}-check">
    </div>
  </div>`;
  containerItems.insertAdjacentHTML('beforeend', html);
});

const saveButton = '<button class="btn-save" type="submit">Save changes</button>';
containerItems.insertAdjacentHTML('beforeend', saveButton);

function saveOptions() {
  items.forEach(saveItems);
}

function recoverOptions() {
  items.forEach(gettingItems);
}

document.addEventListener('DOMContentLoaded', recoverOptions);
document.querySelector('form').addEventListener('submit', saveOptions);
