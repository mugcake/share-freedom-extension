/*
 * ------------------------------- The Fediverse -------------------------------
 *
 * Properties:
 * - name
 *   The name of the social network / service (instance). NO whitespaces!
 *
 * - post
 *   The HTTP POST request method. Important: include the Title and Url.
 *
 * - icon
 *   The icon to be displayed in the popup (main) window. Taken from Fork
 *   Awesome.
 *
 * - host
 *   The default host (instance).
 *
 */
import { Instance } from './interface';

export const diaspora: Instance = {
  name: 'Diaspora',
  post: '/bookmarklet?url=Url&title=Title',
  icon: 'diaspora',
  host: 'https://diasp.org',
};

export const friendica: Instance = {
  name: 'Friendica',
  post: '/compose?title=Title&body=Url',
  icon: 'friendica',
  host: 'https://libranet.de',
};

export const gnusocial: Instance = {
  name: 'GNUsocial',
  post: '/?action=newnotice&status_textarea=Title Url',
  icon: 'gnu-social',
  host: 'https://gnusocial.net',
};

export const hubzilla: Instance = {
  name: 'Hubzilla',
  post: '/rpost?body=Title &url=Url',
  icon: 'hubzilla',
  host: 'https://demo.hubzilla.org',
};

export const lemmy: Instance = {
  name: 'Lemmy',
  post: '/create_post?url=Url&title=Title',
  icon: 'comments',
  host: 'https://lemmy.glasgow.social',
};

export const mastodon: Instance = {
  name: 'Mastodon',
  post: '/share?text=Title&url=Url',
  icon: 'mastodon',
  host: 'https://mastodon.social',
};

export const pleroma: Instance = {
  name: 'Pleroma',
  post: '/share?message=Title Url',
  icon: 'pleroma',
  host: 'https://blob.cat',
};

export const socialhome: Instance = {
  name: 'Socialhome',
  post: '/bookmarklet?url=Url&title=Title',
  icon: 'social-home',
  host: 'https://socialhome.network',
};

export const xmpp: Instance = {
  name: 'XMPP',
  post: '?message=Title Url',
  icon: 'xmpp',
  host: 'https://suchat.org',
};
