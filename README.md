[![jest](https://jestjs.io/img/jest-badge.svg)](https://github.com/facebook/jest) 
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier) 
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](https://gitlab.com/mugcake/fedishare/pulls)
[![Awesome Humane Tech](https://raw.githubusercontent.com/humanetech-community/awesome-humane-tech/main/humane-tech-badge.svg?sanitize=true)](https://github.com/humanetech-community/awesome-humane-tech)

# ![img](https://gitlab.com/uploads/-/system/project/avatar/16473487/logo-min.png) Fedishare

Share the current tab on the [fediverse](https://fediverse.party/).

## Get the extension

| 1. Firefox Add-ons | 2. Abrowser Add-ons |
---|---
[![img](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Firefox_logo%2C_2019.svg/62px-Firefox_logo%2C_2019.svg.png)](https://addons.mozilla.org/en-US/firefox/addon/fedishare-addon) | [![img](https://trisquel.info/files/abrowser_1.png)](https://trisquel.info/en/browser/addons/fedishare)

The second one is the recommended option for users of [GNU IceCat](https://www.gnu.org/software/gnuzilla/), [Iceweasel](https://wiki.parabola.nu/Iceweasel), [Abrowser](https://trisquel.info/en/wiki/abrowser-help) and other 100% free/libre mozilla-based browsers. Or for those who are concerned about their privacy/security and want to avoid any kind of telemetry from Mozilla and third parties.

## Features

Share web pages right from the toolbar button. It's an excelent alternative to the sharing buttons/plugins that are embedded in websites.

**Only supports FOSS and Descentralize projects.**

### Services/protocols:

- [ ] [Bonfire](https://bonfirenetworks.org/)
- [x] [Diaspora](https://diasporafoundation.org/)
- [x] [Ecko](https://magicstone.dev/) (Mastodon button) 
- [ ] [Epicyon](https://epicyon.net/)
- [ ] [Flockingbird](https://flockingbird.social/)
- [x] [Friendica](https://friendi.ca/)
- [x] [Glitch](https://glitch-soc.github.io/docs/) (Mastodon button)
- [x] [Gnusocial](https://gnusocial.network/)
- [x] [GoToSocial](https://docs.gotosocial.org/en/latest/) (Mastodon button)
- [x] [Groundpolis](https://github.com/Groundpolis/Groundpolis) (Mastodon button)
- [x] [Hometown](https://github.com/hometown-fork/hometown) (Mastodon button)
- [ ] [Honk](https://humungus.tedunangst.com/r/honk)
- [x] [Hubzilla](https://hubzilla.org/)
- [x] [Kepi](https://gitlab.com/marnanel/chapeau) (Mastodon button)
- [ ] [Ktistec](https://github.com/toddsundsted/ktistec)
- [x] [Lemmy](https://join-lemmy.org/)
- [x] [Mastodon](https://joinmastodon.org/)
- [x] [Misskey](https://misskey-hub.net/en/) (Mastodon button)
- [x] [Misty](https://zotlabs.com/misty/) (Hubzilla button)
- [ ] [Mobilizon](https://joinmobilizon.org/en/)
- [ ] [NextCloud](https://nextcloud.com/)
- [x] [Osada](https://codeberg.org/zot/osada) (Hubzilla button)
- [ ] [Pjuu](https://pjuu.com/) *1
- [x] [Pleroma](https://pleroma.social/)
- [ ] [Pump.io](http://pump.io/) *2
- [ ] [Smithereen](https://github.com/grishka/Smithereen)
- [x] [Socialhome](https://socialhome.network/)
- [x] [XMPP](https://xmpp.org/)
- [x] [Zap](https://zotlabs.com/zap/) (Hubzilla button)

- *1 It doesn't have a public API
- *2 Conflicts with the [API](https://github.com/pump-io/pump.io/blob/master/API.md)

## Permissions / Privacy

This extension doesn't require any permissions.

It only accesses the Title and URL of the current (active) tab of the browser. The only data needed is the URL of your instance(s), and it is stored locally on your browser/computer.

## Screenshots

| GNU IceCat / Firefox-ESR | Abrowser / Firefox Standard |
---|---
![](https://addons.mozilla.org/user-media/previews/full/265/265482.png) | ![](https://addons.mozilla.org/user-media/previews/full/265/265483.png)

## Contributing

You can contribute in the following ways:

-   Finding and reporting bugs.
-   Contributing code to the project by fixing bugs or implementing features.


### Bug reports

Bug reports and feature suggestions can be submitted to [Gitlab Issues](https://gitlab.com/mugcake/fedishare/issues). Please make sure that you are not submitting duplicates, and that a similar report or request has not already been resolved or rejected in the past using the search function. Please also use descriptive, concise titles.


### Pull request

Please use clean, concise titles for your pull requests.

## Build

This project uses the [Web Extension Boilerplate](https://github.com/davidnguyen179/web-extension-boilerplate).

### Prerequisites

- Mozilla-based browser: 59 (or higher) 

**Install all dependencies:** `npm i`

### Development

- Dev: `npm run app:dev`
- Production: `npm run app:dist`

#### Load package to browsers

##### Automatically

Just execute: `npm run start:browser`

##### Manually

1. Go to the browser's URL address bar
2. Enter `about:debugging#/runtime/this-firefox`
3. Click **Load Temporary Add-on...** 
4. Browse to your `manifest.json` & click **Open**

#### CSS

[tailwindcss](https://tailwindcss.com) is the framework used for this project.

- Dev: `npm run style-dev`
- Production: `npm run style-dist`

Set all css custom in: `public/styles.css`. The `main.css` is generated in `dist`.

## Donations

If you like the stuff I make and are financially able please consider donating. Your support allows me to dedicate more time to this and other FOSS projects :D

<a href='https://ko-fi.com/mugcake' target='_blank'><img height='35' style='border:0px;height:46px;' src='https://uploads-ssl.webflow.com/5c14e387dab576fe667689cf/5cbed8a433a3f45a772abaf5_SupportMe_blue-p-500.png' border='0' alt='Buy Me a Coffee at ko-fi.com' />

[![img](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/mickie/)

__Monero XMR:__ 487VgBpiQhTarmVCVgKMbj9xufMJddDp2dTPFmacoBZDJJYzuicCxHQj9Fk2uNqv7z9moLV83i3jdLBv7EN368tq622rA4B

__Ethereum ETH:__ 0x5dD6Be1C862fB79c128ca6A66bca261604b47F65

## License

[![Large GPLv3 logo with “Free as in Freedom”](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](http://www.gnu.org/licenses/gpl-3.0.en.html)

    Copyright (C) 2019-2021 Miguel (aka mickie) <millet@tuta.io>
    This file is part of Fedishare.
    Fedishare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


